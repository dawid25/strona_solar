from flask import Flask, render_template, url_for, request
from datetime import datetime
from influxdb_client.client.influxdb_client import InfluxDBClient


app = Flask(__name__)

@app.route('/wykresy')
def index():
    # You can generate a Token from the "Tokens Tab" in the UI
    token = "3hcKxNyht5xge7LudFT7B5J7S4vEHi8ByR7NeB6q9jiRN_IQOGTrGitF8yUC28PZjsspRELcoxzzLTzvu3zb5g=="
    org = "Solar"
    bucket = "celka"

    client = InfluxDBClient(url="http://s.aghsolarboat.pl:8086", token=token)

    #query data from influxdb
    query = """from(bucket: "solar")
  |> range(start: -5m)
  |> filter(fn: (r) => r["_measurement"] == "cpu")
  |> filter(fn: (r) => r["_field"] == "usage_user")
  |> filter(fn: (r) => r["cpu"] == "cpu3")
  |> yield(name: "mean")"""

    
    tables = client.query_api().query(query, org=org)

    results = []
    for table in tables:
        for record in table.records:
            results.append((record.get_value()))
            pom = results[:11]

    print(len(pom))        
    return render_template('INFLUX.html', value = pom)

@app.route('/loggin')
def loggin():
    return render_template('index.html')


@app.route('/')
def trasa():
    return render_template('TRASA-CELKI.html')  

@app.route('/stream')
def stream():
    return render_template('STREAM.html')  

@app.route('/logout')
def logout():
    return render_template('WYLOGUJ.html') 
       
if __name__ == "__main__":
    app.run(host='0.0.0.0')
